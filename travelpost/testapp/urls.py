from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'testapp.views.test'),
    url(r'^leh/$', 'testapp.views.leh'),
    url(r'^path/$', 'testapp.views.path'),
    url(r'^localroute/$', 'testapp.views.localroute'),
    url(r'^culture/$', 'testapp.views.culture_photos'),
    url(r'^culture/(?P<pagenum>\d+)/$', 'testapp.views.culture_photos'),
    url(r'^culture/wikicat/$', 'testapp.views.wiki_categories'),
)
