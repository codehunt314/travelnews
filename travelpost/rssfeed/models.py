from django.db import models
import feedparser
from time import mktime
from datetime import datetime


class RSSFeed(models.Model):
	feedurl		=	models.TextField()
	updatedon	=	models.DateTimeField(blank=True, null=True)
	source 		=	models.URLField(blank=True, null=True)
	favicon		=	models.URLField(blank=True, null=True)
	twitter		=	models.CharField(max_length=255, blank=True, null=True)
	facebook	=	models.CharField(max_length=255, blank=True, null=True)
	desc		=	models.TextField(blank=True, null=True)
	remark 		=	models.TextField(blank=True, null=True)

	def update_feed(self):
		feed = feedparser.parse(self.feedurl)
		for each_feed_entry in feed['entries']:
			if each_feed_entry.get('published_parsed', None):
				dt 			= 	datetime.fromtimestamp(mktime(each_feed_entry['published_parsed']))
			else:
				dt 			=	None
			link 			= 	each_feed_entry['link']
			entryid 		= 	each_feed_entry['link']
			title 			= 	each_feed_entry['title']
			if each_feed_entry.get('content', None):
				summary		=	each_feed_entry.get('content')[0].value
			else:
				summary 	= 	each_feed_entry['summary']
			if dt:
				post_date = dt.date()
			else:
				post_date = None
			post, created 	=	Post.objects.get_or_create(url=entryid, rssfeed=self, defaults={'title': title, 'summary': summary, 'posted_on': dt, 'posted_date': post_date})
			post.summary	=	summary
			post.save()


class Post(models.Model):
	url 		=	models.TextField()
	title		=	models.TextField(blank=True, null=True)
	summary		=	models.TextField(blank=True, null=True)
	posted_on	=	models.DateTimeField(blank=True, null=True)
	rssfeed 	=	models.ForeignKey(RSSFeed)
	posted_date	=	models.DateField(blank=True, null=True)


def addRSSFeed(feedurl):
	feed 	= feedparser.parse(feedurl)
	source 	= feed.feed.get('link', None)
	updated_parsed = feed.get('updated_parsed', None)
	if updated_parsed:
		updatedon = datetime.fromtimestamp(mktime(updated_parsed))
	else:
		updatedon = None
	rssfeed, creatrd = RSSFeed.objects.get_or_create(feedurl=feedurl, defaults={'updatedon': updatedon, 'source': source, 'favicon': None, 'twitter': None, 'facebook': None, 'desc': None, 'remark': None})
	#rssfeed.update_feed()

def initRSSFeed():
	from feed_list import feed_list
	for feedurl in feed_list:
		addRSSFeed(feedurl)



"""

import feedparser
from time import mktime
from datetime import datetime

feedparser.parse("http://www.bbc.com/travel/feed.rss").feed

def parse_feed(feed_url):
	feed = feedparser.parse(feed_url)
	for each_feed_entry in feed['entries']:
		dt = datetime.fromtimestamp(mktime(each_feed_entry['published_parsed']))
		link = each_feed_entry['link']
		title = each_feed_entry['title']
		print dt, title, link
	print "\n"


feed_url = ["http://www.bbc.com/travel/feed.rss", "http://www.vagablogging.net/feed"]


feed_url = "http://feeds.feedburner.com/AbandonTheCube"
feed = feedparser.parse(feed_url)
for each_feed_entry in feed['entries']:
	dt = datetime.fromtimestamp(mktime(each_feed_entry['published_parsed']))
	link = each_feed_entry['link']
	title = each_feed_entry['title']
	print dt, title, link
print "\n"


from rssfeed.models import *
for i in RSSFeed.objects.all():
    try:
        i.update_feed()
        print "updated: %s" % (i.feedurl)
    except:
        print "ERROR: %s" % (i.feedurl)


/home/.virtualenv/travelnews.23spaces.com/bin/python /home/www/travelnews.23spaces.com/travelpost/manage.py updatefeed > /dev/null
"""