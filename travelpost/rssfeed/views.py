from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from datetime import datetime, timedelta

from rssfeed.models import *
# Create your views here.


def home(request, page_num=1):
    #return HttpResponseRedirect('/today/')
    td = datetime.today()
    today_post = Post.objects.filter(posted_date__range=(td-timedelta(1), td))
    paged_posts = Paginator(today_post, 10)
    paginated_posts = paged_posts.page(page_num)
    return render_to_response('base.html', {'paginated_posts': paginated_posts, 'urlpath': '/today/'}, context_instance=RequestContext(request))

def blogHome(request, blogId, page_num=1):
    #return HttpResponseRedirect('/today/')
    td = datetime.today()
    today_post = Post.objects.filter(posted_date__range=(td-timedelta(1), td))
    paged_posts = Paginator(today_post, 10)
    paginated_posts = paged_posts.page(page_num)
    return render_to_response('base.html', {'paginated_posts': paginated_posts, 'urlpath': '/today/'}, context_instance=RequestContext(request))


def generic(request, day='today', page_num=1):
    td = datetime.today()
    if day=='today':
        date_range = (td-timedelta(1), td)
        post = Post.objects.filter(posted_date__range=date_range).order_by('-posted_on')
        urlpath = '/%s/'%(day)
    elif day=='yesterday':
        date_range = (td-timedelta(2), td-timedelta(1))
        post = Post.objects.filter(posted_date__range=date_range).order_by('-posted_on')
        urlpath = '/yesterday/'
    elif day=='old':
        old_date = datetime.today() - timedelta(2)
        post = Post.objects.filter(posted_date__lte=old_date).order_by('-posted_on')
    else:
        date_range = (td-timedelta(1), td)
        post = Post.objects.filter(posted_date__range=date_range).order_by('-posted_on')

    urlpath = '/%s/'%(day)
    paged_posts = Paginator(post, 30)
    paginated_posts = paged_posts.page(page_num)
    #import pdb; pdb.set_trace()
    return render_to_response('home.html', {'paginated_posts': paginated_posts, 'urlpath': urlpath}, context_instance=RequestContext(request))


"""
def find_similar_images(userpath, hashfunc = average_hash):
    import os
    def is_image(filename):
        f = filename.lower()
        return f.endswith(".png") or f.endswith(".jpg") or f.endswith(".jpeg") or f.endswith(".bmp") or f.endswith(".gif")

    image_filenames = [os.path.join(userpath, path) for path in os.listdir(userpath) if is_image(path)]
    images = {}
    for img in sorted(image_filenames):
        hash = hashfunc(Image.open(img))
        images[hash] = images.get(hash, []) + [img]
    
    for k, img_list in images.iteritems():
        if len(img_list) > 1:
            print " ".join(img_list)
"""