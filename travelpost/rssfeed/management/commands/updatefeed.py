from django.core.management.base import BaseCommand, CommandError
from rssfeed.models import *

class Command(BaseCommand):
    args = ''
    help = 'Update feed'

    def handle(self, *args, **options):
        for i in RSSFeed.objects.all():
            try:
                i.update_feed()
                self.stdout.write("UPDATED: %s" % i.feedurl)
            except:
                self.stdout.write("ERROR: %s" % i.feedurl)

