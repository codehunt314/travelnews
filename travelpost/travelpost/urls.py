from django.conf.urls import patterns, include, url
from travelpost import settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url(r'^test/', include('testapp.urls')),
	url(r'^', include('rssfeed.urls')),
    # Examples:
    # url(r'^$', 'travelpost.views.home', name='home'),
    # url(r'^travelpost/', include('travelpost.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
)
