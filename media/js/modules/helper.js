/*jshint curly: false, multistr:true */
/*global define:false, */

define(['jquery', 'underscore'],
function($, _){
	'use strict';

	/*
	This Section is for HumanizeDuration
	 */
	var toMillisecond = {
		'milliseconds' : 1,
		'seconds'	: 1000,
		'minutes'	: 1000 * 60,
		'hours'		: 1000 * 60 * 60,
		'days'		: 1000 * 60 * 60 * 24,
		'years'		: 1000 * 60 * 60 * 24 * 365
	};
	/**
	 * Helper Function to convert unit / value to its readable string format
	 * @param  {string} unit  'second', 'minutes', 'hour' etc.
	 * @param  {number} value numerical value of that unit
	 * @return {string}       readable string format, such as
	 * ('second', 24) : 23 second
	 * ('second', 0)  : '' (empty string)
	 * ('second', 1)  : 1 second
	 */
	function humanize(unit, value){
		return value > 0 ? (value + ' ' + unit + ',') : '' ;
	}
	function relativeTimeDiff(milliseconds) {
		/*var seconds = round(Math.abs(milliseconds) / 1000),
			minutes = round(seconds / 60),
			hours = round(minutes / 60),
			days = round(hours / 24),
			years = round(days / 365);*/
		var years = parseInt( milliseconds / toMillisecond.years, 10);
		milliseconds -= years * toMillisecond.years;
		var	days = parseInt( milliseconds / toMillisecond.days, 10);
		milliseconds -= days * toMillisecond.days;
		var	hours = parseInt( milliseconds / toMillisecond.hours, 10);
		milliseconds -= hours * toMillisecond.hours;
		var	minutes = parseInt( milliseconds / toMillisecond.minutes, 10);
		milliseconds -= minutes * toMillisecond.minutes;
		var	seconds = parseInt( milliseconds / toMillisecond.seconds, 10);
		milliseconds -= seconds * toMillisecond.seconds;

		var str =	humanize('year', years) +
					humanize('day', days) +
					humanize('hour', hours) +
					humanize('min', minutes) +
					humanize('sec', seconds);
		var ret = _.first(_.initial(str.split(',')), 2);
		return ret.join(', ');
	}
	/*
	End : Humanize Duration
	 */


	return {
		apiCall : function(type, data, callback, errorCallback){
			$.ajax({
				url : '/site_media/html/' + type + '.json',
				dataType: 'json',
				data : data,
				success : callback,
				error : errorCallback
			});
		},
		humanizeDuration: function(value, unit){
			unit = unit || 'milliseconds';
			return relativeTimeDiff(toMillisecond[unit] * value);
		},
		getCookie: function(name) {
			var cookieValue = null;
			if (document.cookie && document.cookie !== '') {
				var cookies = document.cookie.split(';');
				for (var i = 0; i < cookies.length; i++) {
					var cookie = cookies[i].trim(); //jQuery.trim(cookies[i]);
					// Does this cookie string begin with the name we want?
					if (cookie.substring(0, name.length + 1) == (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
		}
	};
});
