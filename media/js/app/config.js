// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file.
  deps: ['main'],
  baseUrl: '/site_media/js/app',
  paths: {
    // 'main' : '../main-built.min',

    // JavaScript folders.
  

    // Libraries
    'jquery'                 : '../vendor/jquery-2.0.3',
    'underscore'             : '../vendor/underscore',
    'backbone'               : '../vendor/backbone',
    'backbone-relational'    : '../vendor/backbone-relational-dev',
    'backbone-layoutmanager' : '../vendor/backbone.layoutmanager.0.9.3',
    'backbone-package'       : '../vendor/backbone-packages',


    // 'nprogress'              : '../../plugins/nprogress/nprogress',

    // Plugins
    'text'                   : '../plugins/text',
    'domready'               : '../plugins/domReady',
    // 'async'                  : '../plugins/async',

    'templates'              : 'templates'

    // 'mediaRoot'           : '/site_media'
    // 'basePath' : '../../../.'
  },

  shim: {

    jquery: {
      exports: 'jQuery'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: [ 'underscore', 'jquery' ],
      exports: 'Backbone'
    },

    'backbone-relational': ['backbone'],

    'backbone-layoutmanager': {
      deps: ['backbone'],
      exports: 'Backbone.Layout'
    }

  }

});
