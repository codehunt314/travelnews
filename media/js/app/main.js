/*global require:false, console:false */
require([
	'jquery',
	'underscore',
	'backbone-package',
	'../modules/helper',
	'app',
	'router'
],
function ($,_,Backbone, helper, app, Router) {
	'use strict';
	// Define your master router on the application namespace and trigger all
	// navigation from this instance.
	app.router = new Router();

	// Trigger the initial route and enable HTML5 History API support, set the
	// root folder to '/' by default.  Change in app.js.
	Backbone.history.start({
		pushState: true,
		root: app.root
	});

	Backbone.emulateHTTP = true;
	// Modifying Backbone Sync module
	//
	var oldSync = Backbone.sync;
	Backbone.sync = function(method, model, options){
		options.beforeSend = function(xhr){
			xhr.setRequestHeader('X-CSRFToken', helper.getCookie('csrftoken'));
		};
		return oldSync(method, model, options);
	};
	var oldAjax = Backbone.ajax;
	Backbone.ajax = function(request){
		// NProgress.start();
		var xhr = oldAjax(request);
		xhr.always(function(){
			console.log('request done');
			// NProgress.done();
		});
		return xhr;
	};
	/*
	Backbone.sync = function (method, model, options) {
		var getUrl = function (object) {
			if (!(object && object.url)) return null;
			return _.isFunction(object.url) ? object.url() : object.url;
		};
		// edited following line to accomodate patch requests.
		var obj = options.attrs || model.toJSON(options); //model.toJSON();
		obj.action = method;
		// This is Django Related stuff
		options.beforeSend = function (xhr) {
			xhr.setRequestHeader('X-CSRFToken', helper.getCookie('csrftoken'));
		};
		var params;

		if (method === 'read') {
			params = _.extend({
				type: 'GET',
				dataType: 'json'
			}, options);
		} else {
			params = _.extend({
				type: 'POST',
				data: obj,
				dataType: 'json'
			}, options);
		}

		if (!params.url) {
			params.url = getUrl(model);
			if (params.url.indexOf('?') >= 0) {
				params.url = params.url + '&method=' + method;
			} else {
				params.url = params.url + '?method=' + method;
			}
		}
		var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
		model.trigger('request', model, xhr, options);
		return xhr;
	};*/

	// All navigation that is relative should be passed through the navigate
	// method, to be processed by the router. If the link has a `data-bypass`
	// attribute, bypass the delegation completely.
	$(document).on('click', 'a[href]:not([data-bypass])', function (evt) {
		// Get the absolute anchor href.
		var href = {
			prop: $(this).prop('href'),
			attr: $(this).attr('href')
		};
		// Get the absolute root.
		var root = location.protocol + '//' + location.host + app.root;

		// Ensure the root is part of the anchor href, meaning it's relative.
		if (href.prop.slice(0, root.length) === root) {
			// Stop the default event to ensure the link will not cause a page
			// refresh.
			evt.preventDefault();

			// `Backbone.history.navigate` is sufficient for all Routers and will
			// trigger the correct events. The Router's internal `navigate` method
			// calls this anyways.  The fragment is sliced from the root.
			var url = href.attr[0] === '/' ? href.attr.slice(app.root.length + 1) : href.attr;
			Backbone.history.navigate(url, {
				trigger: true
			});
		}
	});

	if (!(window.console && console.log)) {
	    (function() {
	        var noop = function() {};
	        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
	        var length = methods.length;
	        var console = window.console = {};
	        while (length--) {
	            console[methods[length]] = noop;
	        }
	    }());
	}
});