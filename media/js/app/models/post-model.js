/*global define:false */
define(function (require) {
	'use strict';
	var _ = require('underscore'),
		Backbone = require('backbone-package');

	var Post = Backbone.Model.extend({
		initialize: function () {}
	});
	return Post;
});