/*global define:false, JSON:false */
define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app'),
		SharePreview = require('views/sharePreviewView'),
		BlogPostCollection = require('collections/post-collection');

	require('text!templates/blockPost.html');

	var View = Backbone.Layout.extend({
		template : _.template(require('text!templates/blockPost.html')),
		events : {
			// 'click .share-icon': 'sharePost',
			'click .delete-icon': 'deletePost',
		},

		initialize: function(options) {
			console.log('hello');
			this.collection = new BlogPostCollection();
			this.collection.fetch();

			this.modal = app.getModal();
		},
		serialize: function(){
			return { 'blog': false };
		},
		afterRender: function(){
			// $('.content',this.$el).append
		},

		sharePost: function(e){
			var sharePreview = new SharePreview({
				
			});
			this.modal.setContent(sharePreview.render().$el);
			this.modal.show(700);
		},

		deletePost: function(e){
		}
	});
	return View;
});
