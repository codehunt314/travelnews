/*global define:false, JSON:false */
define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app');

	require('text!templates/header.html');

	var View = Backbone.Layout.extend({
		template : _.template(require('text!templates/header.html')),
		events : {
		},

		initialize: function(options) {
			console.log('hello');
		},

		afterRender: function(){
			// $('.content',this.$el).append
		},
	});
	return View;
});
