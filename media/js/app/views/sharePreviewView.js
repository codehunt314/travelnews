/*global define:false, JSON:false */
define(function(require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app');

	require('text!templates/sharePreview.html');

	var View = Backbone.Layout.extend({
		template : _.template(require('text!templates/sharePreview.html')),
		events : {

		},

		initialize: function(options) {
			console.log('shareBlockPost');
		},

		afterRender: function(){
			// $('.content',this.$el).append
		}
	});
	return View;
});
