/*jshint curly: false, multistr:true */
/*global define:false,MEDIA_ROOT:false */

define(function (require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone-package'),
		Modal = require('../plugins/modal');

	// Provide a global location to place configuration settings and module
	// creation.
	var app = {
		// The root path to run the application.
		root: '',
		mediaRoot: MEDIA_ROOT,
		canComment : true,
		API_ROOT : '/api',
		// also check mediaRoot defined in config.js it should be same and defined at both places.
		collections: {},
		models: {},
	};



	var reset = Backbone.Collection.prototype.reset;
	Backbone.Collection.prototype.reset = function(models, options) {
		this.beenReset = true;
		return reset.apply(this, arguments);
	};

	/*var set = Backbone.Model.prototype.set;
	Backbone.Model.prototype.set = function(key, value) {
		var attrs;
		// Handle both `"key", value` and `{key: value}` -style arguments.
		if (typeof key === 'object') {
			attrs = key;
		} else {
			(attrs = {})[key] = value;
		}
		this.beenChanged = this.isNew() ? ( attrs && attrs.id ? true : false ) : true;
		return set.apply(this, arguments);
	};*/

	// Configure LayoutManager with Backbone Boilerplate defaults.
	Backbone.Layout.configure({
		// Allow LayoutManager to augment Backbone.View.prototype.
		// manage: true
		/*,
		// not fetching templates from server that is why, commented this section
		// requirejs would be handling template fetching
		prefix: "app/templates/",

		fetch: function (path) {
			// Concatenate the file extension.
			path = path + ".html";

			// If cached, use the compiled template.
			if (JST[path]) {
				return JST[path];
			}

			// Put fetch into `async-mode`.
			var done = this.async();

			// Seek out the template asynchronously.
			$.get(app.root + path, function (contents) {
				done(JST[path] = _.template(contents));
			});
		}*/
	});

	// Mix Backbone.Events, modules, and layout management into the app object.
	return _.extend(app, {

		// function which check of modal object and returns its reference.
		getModal: function () {
			if (!app.modal) app.modal = new Modal();
			return app.modal;
		},

		// Create a custom object with a nested Views object.
		module: function (additionalProps) {
			return _.extend({
				Views: {}
			}, additionalProps);
		},

		// Helper for using layouts.
		useLayout: function (name, options) {
			// Enable variable arity by allowing the first argument to be the options
			// object and omitting the name argument.
			if (_.isObject(name)) {
				options = name;
			}

			// Ensure options is an object.
			options = options || {};

			// If a name property was specified use that as the template.
			if (_.isString(name)) {
				options.template = name;
			}

			// Create a new Layout with options.
			var layout = new Backbone.Layout(_.extend({
				el: 'body'
			}, options));

			// Cache the refererence.
			this.layout = layout;
			return this.layout;
		}
	}, Backbone.Events);
});