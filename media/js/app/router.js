/*global define:false, Backbone, _:false, console:false */
define( function (require) {
	'use strict';
	var $ = require('jquery'),
		app = require('app'),
		Backbone = require('backbone-package'),
		HeaderView = require('views/headerView'),
		PostsHome = require('views/postListView'),
		BlogHome = require('views/blogPostsView');

	var Router = Backbone.Router.extend({
		initialize: function () {
			// Use main layout and set Views.
			app.useLayout().setViews({
				'.header' : new HeaderView()
			}).render();
		},
	
		routes: {
			''											: 'homePage',
			'blog/:blogId/'								: 'blogPage',
		},
		homePage: function(){
			app.layout.setView('.content', new PostsHome()).render();
		},
		blogPage: function(){
			app.layout.setView('.content', new BlogHome()).render();
		}
	});
	return Router;
});