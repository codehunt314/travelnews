/*global define:false,TRIP_ID:false */
define(function (require) {
	'use strict';
	var _ = require('underscore'),
		Backbone = require('backbone-package'),
		PostModel = require('models/post-model');

	var PostCollection = Backbone.Collection.extend({
		model : PostModel,
		url   : function(models){
			// return '/' + this.options.day + '/'+this.options.pageCount;
			return '/site_media/html/blogList.js';
			
		},
		initialize: function (models, options) {
			this.options = options;
		//	this.tripId = options.tripId;
		},
		parse: function(data) {
            data =  data;
            return data;
        }
	});
	return PostCollection;
});
