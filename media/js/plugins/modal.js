/*jshint curly: false, multistr:true */
/*global define:false,Modernizr:false */
define(['jquery', 'underscore'],
function($, _){
	'use strict';
	var Modal = function(options){
		return this.initialize.call(this, options);
	};

	Modal.prototype = {
		defaults : {
			top:-500,
			actionClass:'action',
			zIndex: 1000
		},
		initialize: function(options){
			this.options = $.extend({}, this.defaults, options);
			this.initUI();
			return this;
		},
		initUI:function(){
			this.divBG = $('<div />',{
				'class':'modalAnimation light-bg hide',
				css : {
					zIndex : this.options.zIndex
				}
			}).on('click', _.bind(function(e){
				if($(e.target).hasClass('light-bg') && this.canHideOnClick)
					this.hide();
				}, this))
				.appendTo(document.body);


			this.divContent = $('<div />',{
				'class' : 'modalBox',
				'html' : '<div class="modal-container"></div>',
				css : {
					zIndex : this.options.zIndex*2
				}
			});
			this.divContent.appendTo(this.divBG);
			this.divContainer = $('div.modal-container', this.divContent);
			this.options.element && this.setContent(this.options.element);

		},
		setContent:function(html, hideCallback){
			this.divContainer.html('');
			var content = $(html);
			content.appendTo(this.divContainer);
			//this.divContent.css('width', content.width());
			this.divContent.off('click');
			this.divContent.on('click','.close_modal',_.bind(function(e){
				e.stopPropagation();e.preventDefault();
				this.hide();
			}, this));
			this.hideCallback = hideCallback;
			//this.content.show();
			//$('a.close_modal',this.divContent).click();
			return this;
		},
		show: function(width, makeBgInactive){

			if(makeBgInactive){
				this.canHideOnClick = false;
			}else{
				this.canHideOnClick = true;
			}


			if($(document.body).hasClass('theaterMode')){
				this.modalOnTop = true;
			}else{
				$(document.body).addClass('theaterMode');
				this.modalOnTop = false;
			}

			this.divBG.addClass('shown');
			setTimeout($.proxy(function(){
				this.divBG.removeClass('hide');
			}, this), 10);

			width = width || '';
			if(width === 'full'){
				this.divContent.css('width', '1130px');
				this.divContent.css('height', '90%');
			}
			else{
				this.divContent.css('width', width);
				this.divContent.css('height', 'auto');
			}


			// this.divBG.fadeIn(1000);
			// removing hide on escape as it was getting difficult to handle in case of multi level modals.
			//
			/*$(document).on('keydown', _.bind(function(event){
				if(event.which===27){
					this.hide();
				}
			}, this));*/
			return this;
		},
		hide:function(){
			if(!this.modalOnTop){
				$(document.body).removeClass('theaterMode');
			}
			this.divBG.removeClass('shown');
			// $(document).off('keydown');
			if(this.hideCallback){
				this.hideCallback.call(this);
			}
			this.divContent.off('click');
			return this;
		},
		destroy: function(){
			this.divBG.removeClass('shown');
			setTimeout($.proxy(function(){
				this.divBG.remove();
			}, this), 300);
		}
	};
	return Modal;
});
